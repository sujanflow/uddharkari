//
//  ProfileViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 19/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "ProfileViewController.h"
#import "KYDrawerController.h"
#import "EditProfileViewController.h"
#import "ServerManager.h"
#import "LoginViewController.h"
#import "ChangePasswordViewController.h"

@interface ProfileViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
    NSMutableDictionary *userInfo;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UITapGestureRecognizer *profileTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapProfilePic:)];

    [self.profilePicImageView addGestureRecognizer:profileTapRecognizer];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [[ServerManager sharedManager] getCurrentProfileDetails:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        NSLog(@"resultDataDic %@",resultDataDic);
        
        self->userInfo = [resultDataDic objectForKey:@"profile"];
        
        self.firstname.text = [NSString stringWithFormat:@"First name: %@",[[resultDataDic objectForKey:@"profile"]objectForKey:@"respondent_first_name"]] ;
        self.lastName.text = [NSString stringWithFormat:@"Last name: %@",[[resultDataDic objectForKey:@"profile"]objectForKey:@"respondent_last_name"]];
        self.age.text =[NSString stringWithFormat:@"Age: %@", [[resultDataDic objectForKey:@"profile"]objectForKey:@"respondent_age"]];
        self.contactNo.text = [NSString stringWithFormat:@"Contact Number: %@",[[resultDataDic objectForKey:@"profile"]objectForKey:@"respondent_contact_number"]];
        self.designation.text = [NSString stringWithFormat:@"Designation: %@",[[resultDataDic objectForKey:@"profile"]objectForKey:@"respondent_designation"]];
        //self.gender.text = [NSString stringWithFormat:@"%@",[[resultDataDic objectForKey:@"profile"]objectForKey:@"respondent_gender"]];
      
        
        
    }];
    
    [[ServerManager sharedManager] getProfilePic:^(BOOL success, UIImage *image) {
        
        NSLog(@"resultDataDic...... %@",image);
        
        self.profilePicImageView.image = image;
        
    }];
}

- (IBAction)menuButtonAction:(id)sender {
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
}

- (IBAction)editbuttonAction:(id)sender {
    
    EditProfileViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    
    viewController.designation = [userInfo objectForKey:@"respondent_designation"];
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}
    
- (IBAction)changePassButtonAction:(id)sender {
    
    ChangePasswordViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    
   
    [self.navigationController pushViewController:viewController animated:YES];
    
}
    
- (IBAction)logoutButtonAction:(id)sender {
    
    [[ServerManager sharedManager] logOut:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        if (success) {
            
            NSLog(@"logOut");
            
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"isLoggedIn"];
            
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            LoginViewController *loginVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"LoginViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginVC];
            navController.navigationBar.hidden = YES;
            
            UIWindow *window = [UIApplication sharedApplication].delegate.window;
            window.rootViewController = navController;
            
        }
    }];
}

- (void)tapProfilePic:(UITapGestureRecognizer*)sender {
    
    
    NSLog(@"tap profile pic");
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}
    
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.profilePicImageView.image = chosenImage;
    
    NSData *data = UIImageJPEGRepresentation(self.profilePicImageView.image, 0.8);
    
    [[ServerManager sharedManager]uploadProPicWithImageData:data withCompletionBlock:^(BOOL success) {
        
        
        NSLog(@".............");
    }];

    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
    

@end
