//
//  AppDelegate.m
//  Uddharkari Police
//
//  Created by Md.Ballal Hossen on 18/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "AppDelegate.h"
@import Firebase;
#import "KYDrawerController.h"
#import "ServerManager.h"

@interface AppDelegate ()<FIRMessagingDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"isLoggedIn"] != nil) {
        
        if( [[[NSUserDefaults standardUserDefaults]objectForKey:@"isLoggedIn"]intValue] == 1){
            
            KYDrawerController *drawerController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"KYDrawerController"]; //or the homeController
            //            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:homeController];
            self.window.rootViewController = drawerController;
            
        }
        
    }
    //firebase config
    
#pragma mark  configure_Firebase
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    
#pragma mark  for_get_device_token_and_push_notification
    if ([UNUserNotificationCenter class] != nil) {
        
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    
    return YES;
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"APNs device token retrieved: %@", deviceToken);
    [FIRMessaging messaging].APNSToken = deviceToken;
    
    NSString *deviceTokenString = [[NSString stringWithFormat:@"%@",deviceToken] stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceTokenString = [deviceTokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    
    NSString *fcmToken = [FIRMessaging messaging].FCMToken;
    
    
    NSLog(@"deviceToken %@",deviceTokenString);
    NSLog(@"refreshedToken %@",fcmToken);
    
   
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenString forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] setValue:fcmToken forKey:@"registrationToken"];
    
    
    
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken{
    
    NSLog(@"fcmToken in didReceiveRegistrationToken %@",fcmToken);
    
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"userInfo didReceiveRemoteNotification %@",userInfo);
    

    completionHandler(UIBackgroundFetchResultNoData);
}


//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    
  //  [self generateLocalNotification];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotHelpRequest" object:nil userInfo:notification.request.content.userInfo];
    
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
   // completionHandler(UIBackgroundFetchResultNoData);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    
    NSLog(@"response.notification.request.content.userInfo %@",response.notification.request.content.userInfo);
    
//    if ([response.notification.request.content.userInfo objectForKey:@"gcm.message_id"]) {
//
//
//    }
    
      [[NSNotificationCenter defaultCenter] postNotificationName:@"gotHelpRequest" object:nil userInfo:response.notification.request.content.userInfo];
    
   
    completionHandler();
}

- (void)generateLocalNotification {
    
    UNMutableNotificationContent *localNotification = [UNMutableNotificationContent new];
    localNotification.title = [NSString localizedUserNotificationStringForKey:@"SOS-BROADCAST" arguments:nil];
    localNotification.body = [NSString localizedUserNotificationStringForKey:@"A user need help!" arguments:nil];
    localNotification.sound = [UNNotificationSound soundNamed:@"loud_siren.caf"];
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1.0f repeats:NO];
    
    //localNotification.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] +1);
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Time for a run!" content:localNotification trigger:trigger];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        
        NSLog(@"Notification created");
        
    }];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
