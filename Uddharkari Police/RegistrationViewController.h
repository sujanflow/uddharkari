//
//  RegistrationViewController.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQDropDownTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegistrationViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UITextField *firstnameTestField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *genderTextField;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *stationTextField;


@property (weak, nonatomic) IBOutlet UITextField *contactNoTF;

@property (weak, nonatomic) IBOutlet IQDropDownTextField *designationTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTF;




@end

NS_ASSUME_NONNULL_END
