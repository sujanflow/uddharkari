//
//  LoginViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 1/4/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "LoginViewController.h"
#import "ServerManager.h"
#import "KYDrawerController.h"
#import "RegistrationViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.phoneNoTextField.text = @"+8801687414311";
    self.passwordTextField.text = @"respondent16";
}


- (IBAction)loginButtonAction:(id)sender {
    
    [[ServerManager sharedManager] postLoginWithPhone:self.phoneNoTextField.text password:self.passwordTextField.text completion:^(BOOL success) {
        
        if (success) {
            
            NSLog(@"success");
            
            
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"isLoggedIn"];
            [[NSUserDefaults standardUserDefaults] setValue:self.phoneNoTextField.text forKey:@"phoneNo"];
            [[NSUserDefaults standardUserDefaults] setValue:self.passwordTextField.text forKey:@"passWord"];

            
            UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:Nil];
            KYDrawerController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"KYDrawerController"];
            UIWindow *window = [UIApplication sharedApplication].delegate.window;
            window.rootViewController = destinationVC;
            
            
            
        }
        
        
    }];
    
}

- (IBAction)registerButtonAction:(id)sender {
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:Nil];
    RegistrationViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
    
    [self.navigationController pushViewController:destinationVC animated:YES];
    
}


@end
