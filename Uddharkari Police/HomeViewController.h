//
//  HomeViewController.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *notificationView;

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *age;
@property (weak, nonatomic) IBOutlet UITextField *emgContactNo;
@property (weak, nonatomic) IBOutlet UITextField *emgContactRelation;

@property (weak, nonatomic) IBOutlet UIButton *rescueButton;
@property (weak, nonatomic) IBOutlet UIButton *caseCompleteButton;


@end

NS_ASSUME_NONNULL_END
