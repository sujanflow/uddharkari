//
//  ServerManager.h
//  ArteVue
//
//  Created by Tanvir Palash on 1/4/17.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//
#import <UIKit/UIKit.h>


@interface ServerManager : NSObject

@property (nonatomic, readwrite) BOOL isNetworkAvailable;

+ (ServerManager *)sharedManager;

- (BOOL)checkForNetworkAvailability;

typedef void (^completionImage)(BOOL success, UIImage *image);

typedef void (^completionBlockDic)(BOOL success, NSMutableDictionary *resultDataDic);
typedef void (^completionBlockArray)(BOOL success, NSMutableArray *resultDataArray);

typedef void (^completionBlockWithStatus)(BOOL success);
typedef void (^completionBlockWithString)(BOOL success, NSString* resultString);

-(void) postSignUpWithDictionary:(NSMutableDictionary*)userInfo completion:(completionBlockDic)completion;

-(void) postLoginWithPhone:(NSString*)phoneNo password:(NSString*)password completion:(completionBlockWithStatus)completion;


-(void) getCurrentProfileDetails:(completionBlockDic)completion;
-(void) updateCurrentProfileDetailswith:(NSMutableDictionary*)userInfo withBlock:(completionBlockDic)completion;
-(void) postHistory:(completionBlockDic)completion;
-(void) postFCMToken:(NSMutableDictionary*)info withCompletionBlock: (completionBlockWithStatus)completion;

-(void) uploadProPicWithImageData:(NSData*)imageData withCompletionBlock:(completionBlockWithStatus)completion;
-(void) getProfilePic:(completionImage)completion;


-(void) logOut:(completionBlockDic)completion;
-(void) changePassword:(NSMutableDictionary *)info withCompletionBlock:(completionBlockWithStatus)completion;




-(void) deleteFcmTokenWithCompletion:(completionBlockWithStatus)completion;

-(void) getSpeakerListWith:(completionBlockArray)completion;
-(void) getNotificationListWith:(completionBlockArray)completion;

-(void) getAgendaDataForDay:(int)dayNum withCompletionBlock:(completionBlockDic)completion;

-(void) getDigitalDocumentwithCompletionBlock:(completionBlockArray)completion;
-(void) getContactwithCompletionBlock:(completionBlockArray)completion;

-(void) getBlogCompletionBlock:(completionBlockArray)completion;
-(void) getUserListCompletionBlock:(completionBlockDic)completion;

-(void) postThreadWith:(NSString*)myId OtherId:(NSString*)otherId completion:(completionBlockArray)completion;

-(void) getMessagesWith:(NSString*)threadId OtherId:(NSString*)userId completion:(completionBlockDic)completion;

-(void) getMessageThreadsForId:(NSString*)userId completion:(completionBlockDic)completion;

-(void) postMessageWithDictionary:(NSMutableDictionary*)messageDic completion:(completionBlockDic)completion;

-(void) getTwitteCompletionBlock:(void (^)(NSMutableArray *responseArray))callback;
    
-(void) getBlogImageForUrl:(NSString*)url CompletionBlock:(completionBlockDic)completion;

- (void) downloadFileFromURL: (NSString *) URL saveTo:(NSURL *)saveToPath withProgress:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSURL *filePath))completionBlock onError:(void (^)(NSError *error))errorBlock;



- (void)removeVideoAtPath:(NSURL *)filePath;

@end
