//
//  AppSupport.m
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "Routes.h"
#import "LoginViewController.h"
#import "WebContainerViewController.h"
#import "ParentTabViewController.h"
#import "RegistrationViewController.h"
#import "RegistrationSecondViewController.h"
#import "RegistrationThirdViewController.h"
#import "ResourcesViewController.h"
#import "SpeakerViewController.h"
#import "ContactViewController.h"
#import "ChatViewController.h"
#define MainStoryBoard @"Main"
#define Authentication @"Login"
#define ProfileStoryBoard @"Profile"

@implementation Routes

+(void)setLoaderAsRootViewController
{
    AppDelegate *appDelegate= (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:Nil];
  //  MapViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"MapViewController"];
    appDelegate.window.rootViewController=[homeStoryBoard instantiateInitialViewController];
    [appDelegate.window makeKeyAndVisible];
}


+(void)gotoTabViewFromVC:(UIViewController*)vc
{
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:Nil];
    ParentTabViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"ParentTabViewController"];
    [vc.navigationController pushViewController:destinationVC animated:YES];
}

+(void)gotoLoginViewFromVC:(UIViewController*)vc
{
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:Authentication bundle:Nil];
    LoginViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [vc.navigationController pushViewController:destinationVC animated:YES];
}


+(void)gotoRegistrationViewFromVC:(UIViewController*)vc
{
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:Authentication bundle:Nil];
    RegistrationViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
    [vc.navigationController pushViewController:destinationVC animated:YES];
    

}

+(void)gotoSecondRegistrationViewFromVC:(UIViewController*)vc withDic:(NSMutableDictionary*)datadic
{
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:Authentication bundle:Nil];
    RegistrationSecondViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"RegistrationSecondViewController"];
    destinationVC.dataDicfromFirstView = datadic;
    [vc.navigationController pushViewController:destinationVC animated:YES];
    
    
}


+(void)gotoThirdRegistrationViewFromVC:(UIViewController*)vc withDic:(NSMutableDictionary*)datadic
{
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:Authentication bundle:Nil];
    RegistrationThirdViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"RegistrationThirdViewController"];
    
    destinationVC.dataDicfromSecondView = datadic;
    [vc.navigationController pushViewController:destinationVC animated:YES];
    
    
}


+(void)popToLoginViewControllerFromVC:(UIViewController*)vc
{
    NSArray *array = [vc.navigationController viewControllers];
    for (UIViewController *controller in array) {
        
         if ([controller isKindOfClass:[LoginViewController class]]) {

             
            [vc.navigationController popToViewController:controller
                                                  animated:YES];
            return;
        }
    }
}

+(void)gotoWebContainerViewFromVC:(UINavigationController*)vc forUrlString:(NSString*)urlString
{
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:Nil];
    WebContainerViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"WebContainerViewController"];
    destinationVC.urlString=urlString;
    [vc pushViewController:destinationVC animated:YES];
}

+(void)gotoResourceViewFromVC:(UINavigationController*)vc
{
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:Nil];
    ResourcesViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"ResourcesViewController"];
    [vc pushViewController:destinationVC animated:YES];
    
    
}

+(void)gotoSpeakerViewFromVC:(UINavigationController*)vc
{
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:Nil];
    SpeakerViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"SpeakerViewController"];
    [vc pushViewController:destinationVC animated:YES];
    
    
}
+(void)gotoContactViewFromVC:(UINavigationController*)vc{
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:Nil];
    ContactViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"ContactViewController"];
    [vc pushViewController:destinationVC animated:YES];
    
}

+(void)gotoChatViewFromVC:(UINavigationController*)vc withDictionary:(NSMutableDictionary*)dataDic {
    
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:Nil];
    ChatViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    
    if (dataDic!=nil) {
    
        destinationVC.notiDic = dataDic;
    }
    
    [vc pushViewController:destinationVC animated:YES];
    
}


+(void)gotoLoginViewFrom:(UIViewController*)vc
{
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:Authentication bundle:Nil];
    LoginViewController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:destinationVC];
    navController.navigationBarHidden=YES;
    [vc presentViewController:navController animated:YES completion:nil];
    
}


@end
