//
//  AppSupport.m
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "AppUtility.h"
#import "NSString+Utils.h"

@implementation AppUtility


+(BOOL) mailAuthentication:(NSString *)mailText{
    
    BOOL stricterFilter = NO; 
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:mailText];
}


+(BOOL) phoneAuthentication:(NSString *)phoneText{
    
//    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
//    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//
//    return [phoneTest evaluateWithObject:phoneText];
    
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([phoneText isValid] && [phoneText rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL) fieldIsEmpty:(NSString*) inputString
{
    
    if ([inputString length] == 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}


+(NSDate*) getNSDateFromServerDate:(NSString*)serverDateString
{
    //2016-05-02
    
    NSDateFormatter *datef=[[NSDateFormatter alloc]init];
    [datef setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"]; // Date formater
    NSDate *date = [datef dateFromString:serverDateString];
    
    return date;
}
+(NSString*) getDateStringFromDate:(NSDate*)date
{
//    NSDateFormatter *df = [[NSDateFormatter alloc] init];
//    df.timeStyle = NSDateFormatterShortStyle;
//    df.dateStyle = NSDateFormatterNoStyle;
//    df.doesRelativeDateFormatting = NO;
//    return [df stringFromDate:date];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    
    [dateformate setDateFormat:@"dd MMM , YYYY"]; // Date formater
    NSString *dateString = [dateformate stringFromDate:date]; // Convert date to string
    return dateString.length>0?dateString:@"N/A";
}

+(NSString*) getMyTripStyleDateStringFromDate:(NSString*)serverString
{
    NSDateFormatter *datef=[[NSDateFormatter alloc]init];
    [datef setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]; // Date formater
    NSDate *date = [datef dateFromString:serverString];
    
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    
    [dateformate setDateFormat:@"MMM dd, YYYY, hh.mm a"]; // Date formater
    NSString *dateString = [dateformate stringFromDate:date]; // Convert date to string
    
    return dateString.length>0?dateString:@"N/A";
}


+(NSInteger) nextPageParser:(NSString *)str{
    
   // http://159.65.22.249:24525/api/content/videos/?page=2
    NSArray *items = [str componentsSeparatedByString:@"page="];
    
    NSString *count = [items objectAtIndex:1];
    
    NSInteger page = [count integerValue];
    
    page = page>1 ? page : 1;
    
    return page;
}



+ (NSString *)getPriceTextforCurrency:(NSString*)price
{
    
    if ([price isValid] && ![price isEqualToString:@"SAR"]) {
        
        return [NSString stringWithFormat:@"SAR %@",price];
    }
    
    return @"Price";
}

+ (NSString *)getFirstObjectFromArray:(NSArray*)array
{
    if (![array isEqual:[NSNull null]] && array.count > 0 && [array isKindOfClass:[NSArray class]]) {
        return [[array objectAtIndex:0] objectForKey:@"image"];
    }
    
    return nil ;
}

+ (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution)
    {
        CGFloat ratio = width/height;
        if (ratio > 1)
        {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else
        {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft)
    {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else
    {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


+ (NSMutableDictionary *)extractLocationDataFromString:(NSString *)locationString
{
    NSMutableDictionary* locationDic=[[NSMutableDictionary alloc] init];
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"[,]"];
    NSArray *picUpItems=[locationString componentsSeparatedByCharactersInSet:set];
    
    [locationDic setObject:[picUpItems objectAtIndex:2] forKey:@"latitude"];
    [locationDic setObject:[picUpItems objectAtIndex:1] forKey:@"longitude"];
    
    return locationDic;
}

+ (UIViewController*) topMostViewController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [self topViewControllerWithRootViewController:topController];
}


+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

//+(NSString* ) getCSRFFromCookie{
//    NSArray* cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:API_SERVER_URL]];
//    DLog(@"cookie csrf %@",    [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]);
//    
//    NSString* strCookieValue=@"";
//    for(NSHTTPCookie* cookie in cookies) {
//        if([cookie.name isEqualToString:@"csrftoken"])
//            return cookie.value;
//    }
//    
//    return strCookieValue;
//}

@end
