//
//  ATMConstant.h
//  AlTayerMotors
//
//  Created by Niteco Macmini 5wdwyl  on 10/30/15.
//  Copyright © 2015 Niteco. All rights reserved.
//

#ifndef AppConstant_h
#define AppConstant_h


#define iSnackBarMessage                    @"iSnackBarLib - Example : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."

#define google_app_key @"AIzaSyCFtCVol9vOUj2MDYC9u1bsG0CJ7c-EfUI"
#define flurry_app_id @"TZKDWYSK2RCPFJP8RFQ7"

#define basicFont @"Roboto-Regular"


#define mapGradientColor @"#CCFAFAFA"
#define buttonColor @"#00B4BF"
#define textFieldBorder @"#7D7C7C"
#define textFieldPlaceHolderColor @"#B8B8B8"
#define polyLineColor @"#666666"

#define agendaColor1 @"45B334"
#define agendaColor2 @"A1CC3B"
#define agendaColor3 @"FAA21B"
#define agendaColor4 @"FA691B"

#define blogURL @"http://innovation.brac.net/fif2017/blogs/"
#define speackerNominationURl @"https://docs.google.com/forms/d/e/1FAIpQLSeWl2S9zizybKppYrhnS4LzBNHTf1VgMGeYyzfzX68bcLGpDg/viewform"
#define writeBlogURL @"https://docs.google.com/forms/d/e/1FAIpQLSdLJtKGDPlqdRGSQSaDsZf5CNxNE2SMPcx-9r9wvFbcGDgQjg/viewform"
#define pitchInnovationURL @"https://docs.google.com/forms/d/1YqbjOKWopbkA20WAyqomDcC0Sx7wPQgBQysZShKnNms/edit"



#define clientIdForApi @"LyuFzhCF5LB0BfP03ztSaBUFUlU841QMH8Y0K8hn"
#define clientSecretForApi @"DNOLn464z7m1H9m4J6pwIPCxwZIJJYZ9KWesYnBA5alno5N2uLVzbM5hiC33Ffv3DwJ3NjJ0AIEtoJ8R2OfCPVElBEovM2RMUBz8ctEyL7IvLMBcOjcCmopuoGwPFMgz"

#define scopeValue @"*"
#define grantType @"password"
#define grantTypeForRefreshToken @"refresh_token"


#define devAccessToken  @"THl1RnpoQ0Y1TEIwQmZQMDN6dFNhQlVGVWxVODQxUU1IOFkwSzhobjpETk9MbjQ2NHo3bTFIOW00SjZwd0lQQ3h3WklKSllaOUtXZXNZbkJBNWFsbm81TjJ1TFZ6Yk01aGlDMzNGZnYzRHdKM05qSjBBSUV0b0o4UjJPZkNQVkVsQkVvdk0yUk1VQno4Y3RFeUw3SXZMTUJjT2pjQ21vcHVvR3dQRk1neg=="

#define kReminderDate                       30

#endif /* AppConstant_h */
