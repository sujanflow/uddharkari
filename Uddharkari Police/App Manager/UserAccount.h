//
//  Recipe.h
//  RecipeApp
//
//  Created by Simon on 25/12/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserAccount : NSObject

@property (nonatomic) int userId;
@property (nonatomic) float penaltyAmount;
@property (nonatomic) float rating;

@property (nonatomic, strong) NSString *userImageName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *paymentStatus;

@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSMutableDictionary* location;

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *refreshToken;
@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, strong) NSString *gcmRegKey;

@property (nonatomic, strong) NSString *birthDate;
@property (nonatomic, strong) NSString *gender;

+ (UserAccount *)sharedInstance;

-(NSMutableDictionary *) toNSDictionary;
-(void)removeUser;
-(BOOL)isLoggedIn;

@end
