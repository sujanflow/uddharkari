//
//  AppSupport.m
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "APIEndPoints.h"

@implementation APIEndPoints

const struct BaseURL BaseURL = {
    .Development=@"http://104.192.5.213",
    .Production=@"http://fif2018.jokhontokhon.com",
    .ChatBaseUrl=@"https://fif2018.jokhontokhon.com/"
};

const struct Api Api = {
    
    .registration = @"registerRespondent",
    .signIn=@"respondentLogin",
    
    .ownProfileInfo=@"getRespondentProfile",
    .updateProfileInfo = @"updateRespondentProfile",
    .history = @"getRespondentHistories",
    .updateRespondentLocation = @"updateRespondentLocation",
    
    .fcmRegistration = @"registerRespondentToken",
    .logout = @"respondentLogout",
    .changePassword = @"updateRespondentPassword",
    .uploadProPic = @"uploadRespondentAvatar",
    .downloadProPic = @"getRespondentAvatar",
};

@end
