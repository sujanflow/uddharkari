//
//  ServerManager.m
//  ArteVue
//
//  Created by Tanvir Palash on 1/4/17.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ServerManager.h"

#import "Reachability.h"
#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "APIEndPoints.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "UserAccount.h"
#import "PrefixHeader.pch"


#pragma mark - interface
@interface ServerManager(){
    Reachability *networkReachability;
    
}


@end

#pragma mark - implementation
@implementation ServerManager

@synthesize isNetworkAvailable;

#pragma mark - init
- (id)init{
    if (self = [super init]){
    }
    return self;
}

+(ServerManager *)sharedManager
{
    static ServerManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[ServerManager alloc] init];
    });
    return _sharedInstance;
}



/* ***** API ***** */

#pragma mark -  API - FUNCTIONS


- (void)postSignUpWithDictionary:(NSMutableDictionary*)userInfo completion:(completionBlockDic)completion{
    
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@/",BaseURL.Development,Api.registration];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
       // DLog(@"userInfo %@",userInfo);
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:userInfo forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                    
                    NSLog(@"responseDictionary %@",responseDictionary);
                    
                //    NSDictionary* userDictionary=[responseDictionary dictionaryByReplacingNullsWithBlanks];
                    
                    
                    completion(TRUE,[[responseDictionary dictionaryByReplacingNullsWithBlanks] mutableCopy]);
                    
                    
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(FALSE,nil);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
}

-(void)postLoginWithPhone:(NSString*)phoneNo password:(NSString*)password completion:(completionBlockWithStatus)completion
{
    
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.signIn];
//        NSString *authToken =[NSString stringWithFormat:@"Basic %@",devAccessToken ];
//
        
       
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:phoneNo forKey:@"username"];
        [parameterDic setObject:password forKey:@"password"];
       // [parameterDic setObject:grantType forKey:@"grant_type"];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:parameterDic forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];

                    NSLog(@"responseDictionary %@",responseDictionary);
                    
                    for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies])
                    {
                        NSLog(@"name: '%@'\n",   [cookie name]);
                        NSLog(@"value: '%@'\n",  [cookie value]);
                        NSLog(@"domain: '%@'\n", [cookie domain]);
                        NSLog(@"path: '%@'\n",   [cookie path]);
                        
                        [UserAccount sharedInstance].accessToken=[cookie value];
                        
                    }
                    
                    completion(TRUE);
                    
//                    [UserAccount sharedInstance].refreshToken=[userDictionary objectForKey:@"refresh_token"];
//
//                    [[ServerManager sharedManager] getCurrentProfileDetails:^(BOOL success, NSMutableDictionary *resultDataDic) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
//                            completion(TRUE);
//                        });
//                    }];
                    
                    
                
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}



-(void) logOut:(completionBlockDic)completion
{
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.logout];
        NSString *authToken =[NSString stringWithFormat:@"%@",[UserAccount sharedInstance].accessToken ];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:nil withAccessToken:authToken forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                
                completion(TRUE, [[responseDictionary dictionaryByReplacingNullsWithBlanks] mutableCopy]);
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}
-(void) changePassword:(NSMutableDictionary *)info withCompletionBlock:(completionBlockWithStatus)completion
{
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.changePassword];
        NSString *authToken =[NSString stringWithFormat:@"%@",[UserAccount sharedInstance].accessToken ];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:info forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) postFCMToken:(NSMutableDictionary *)info withCompletionBlock:(completionBlockWithStatus)completion{

  //  [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];

    if ([self checkForNetworkAvailability]) {

        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.fcmRegistration];

        
        NSString *authToken =[NSString stringWithFormat:@"%@",[UserAccount sharedInstance].accessToken ];

        
        NSLog(@"authToken %@ ",authToken);
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);

        dispatch_async(backgroundQueue, ^{

            [self postServerRequestWithParams:info withAccessToken:authToken forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
            
                NSLog(@"FCM responseDictionary %@",responseDictionary);

                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    
                   
                    dispatch_async(dispatch_get_main_queue(), ^{
                    //    [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(TRUE);
                    });

                }else{

                    dispatch_async(dispatch_get_main_queue(), ^{
                     //   [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(FALSE);
                    });

                }

            }];

        });
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) uploadProPicWithImageData:(NSData*)imageData withCompletionBlock:(completionBlockWithStatus)completion{
    
    if ([self checkForNetworkAvailability]) {
        

    NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.uploadProPic];
    
    NSString *authToken =[NSString stringWithFormat:@"%@",[UserAccount sharedInstance].accessToken ];

    
    dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
    
    dispatch_async(backgroundQueue, ^{
        
        [self postServerRequestForImage:imageData WithParams:nil forUrl:httpUrl keyValue:@"avatar" andAccessToken:authToken withResponseCallback:^(NSDictionary *responseDictionary) {
            
            NSLog(@"image responseDictionary %@",responseDictionary);
            
            if ( responseDictionary!=nil) {
                //Valid Data From Server
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //    [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                    completion(TRUE);
                });
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //   [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                    completion(FALSE);
                });
                
            }
            
        }];
    });

}else{
    
    [self showAlertForNoInternet];
}
}

-(void) getProfilePic:(completionImage)completion{
        
        
    if ([self checkForNetworkAvailability]) {
        
        // NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.ChatBaseUrl,Api.userList];
        
        NSString *httpUrl= [NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.downloadProPic];
        
        NSString *authToken =[NSString stringWithFormat:@"%@",[UserAccount sharedInstance].accessToken ];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withAccessToken:authToken withResponseCallback:^(UIImage *responseDictionary) {
                
                NSLog(@"?????image responseDictionary %@",responseDictionary);
                
                if ( responseDictionary!=nil) {
                    
                    //  NSArray *tempArray=(NSArray*)responseDictionary;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        
                        completion(TRUE, responseDictionary);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        
                        completion(FALSE,nil);
                    });
                }
            }];
        });
        
    }else{
        
        [self showAlertForNoInternet];
    }
        
        
        
}

















- (void)deleteFcmTokenWithCompletion:(completionBlockWithStatus)completion
{
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability])
    {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@/%@/",BaseURL.Development,Api.fcmRegistration,[UserAccount sharedInstance].gcmRegKey];
        NSString *authToken =[NSString stringWithFormat:@"Bearer %@",[UserAccount sharedInstance].accessToken ];
        
       // DLog(@"authToken %@",authToken);
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            [self deleteServerRequestForUrl:httpUrl andAccessToken:authToken withResponseCallback:^(NSDictionary *responseDictionary) {
                if ( responseDictionary!=nil) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(TRUE);
                    });
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(FALSE);
                    });
                    
                }
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) getCurrentProfileDetails:(completionBlockDic)completion
{
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.ownProfileInfo];
        NSString *authToken =[NSString stringWithFormat:@"%@",[UserAccount sharedInstance].accessToken ];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:nil withAccessToken:authToken forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                
                completion(TRUE, [[responseDictionary dictionaryByReplacingNullsWithBlanks] mutableCopy]);
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) updateCurrentProfileDetailswith:(NSMutableDictionary*)userInfo withBlock:(completionBlockDic)completion
{
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.updateProfileInfo];
        NSString *authToken =[NSString stringWithFormat:@"%@",[UserAccount sharedInstance].accessToken ];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:userInfo withAccessToken:authToken forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                
                NSLog(@"responseDictionary %@",responseDictionary);
                
                completion(TRUE, [[responseDictionary dictionaryByReplacingNullsWithBlanks] mutableCopy]);
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}


-(void) postHistory:(completionBlockDic)completion
{
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=[NSString stringWithFormat:@"%@/%@",BaseURL.Development,Api.history];
        NSString *authToken =[NSString stringWithFormat:@"%@",[UserAccount sharedInstance].accessToken ];
        
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:nil withAccessToken:authToken forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                
                completion(TRUE, [[responseDictionary dictionaryByReplacingNullsWithBlanks] mutableCopy]);
                
            }];
            
            
        });
        
    }else{
        [self showAlertForNoInternet];
    }
    
}



















-(void) postThreadWith:(NSString*)myId OtherId:(NSString*)otherId completion:(completionBlockArray)completion{
    
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {

        NSString *httpUrl = @"http://fifchat.elevenbd.com/create_new_message_thread.php";
        
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:myId forKey:@"sender_id"];
        [parameterDic setObject:otherId forKey:@"receiver_id"];
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:parameterDic forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                    
                    NSLog(@"responseDictionary %@",responseDictionary);
                    
                    NSMutableArray *responseArray = (NSMutableArray*)responseDictionary;
                    
                    completion(TRUE,responseArray);
                    
                 
                    
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(FALSE,nil);
                    });
                    
                }
                
            }];
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}

-(void) getMessagesWith:(NSString*)threadId OtherId:(NSString*)userId completion:(completionBlockDic)completion{
    
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=@"http://fifchat.elevenbd.com/get_all_message.php";
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];
        
        [parameterDic setObject:threadId forKey:@"thread_id"];
        [parameterDic setObject:userId forKey:@"uid"];
        

        
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withParam:parameterDic withResponseCallback:^(NSDictionary *responseDictionary) {
                
            
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                    
                    NSLog(@"responseDictionary....... %@",responseDictionary);
                    
                //    NSDictionary* userDictionary=[responseDictionary dictionaryByReplacingNullsWithBlanks];
                    
                    
                    completion(TRUE,[responseDictionary mutableCopy]);
                    
                    
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(FALSE,nil);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) getMessageThreadsForId:(NSString*)userId completion:(completionBlockDic)completion{
    
    [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=@"http://fifchat.elevenbd.com/get_all_message_thread.php";
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
        NSMutableDictionary *parameterDic = [[NSMutableDictionary alloc] init];

        [parameterDic setObject:userId forKey:@"uid"];
        
        NSLog(@"parameterDic parameterDic ??????????? %@",parameterDic);
        
        dispatch_async(backgroundQueue, ^{
            
            [self getServerRequestForUrl:httpUrl withParam:parameterDic withResponseCallback:^(NSDictionary *responseDictionary) {
                
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                    
                    NSLog(@"responseDictionary %@",responseDictionary);
                    
                    //    NSDictionary* userDictionary=[responseDictionary dictionaryByReplacingNullsWithBlanks];
                    
                    
                    completion(TRUE,[[responseDictionary dictionaryByReplacingNullsWithBlanks] mutableCopy]);
                    
                    
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                        completion(FALSE,nil);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
}

-(void) postMessageWithDictionary:(NSMutableDictionary*)messageDic completion:(completionBlockDic)completion{
    
    if ([self checkForNetworkAvailability]) {
        
        NSString *httpUrl=@"http://fifchat.elevenbd.com/send_message_v2.php";
        
        dispatch_queue_t backgroundQueue = dispatch_queue_create("Background Queue", NULL);
        
      //  DLog(@"userInfo %@",messageDic);
        dispatch_async(backgroundQueue, ^{
            
            [self postServerRequestWithParams:messageDic forUrl:httpUrl withResponseCallback:^(NSDictionary *responseDictionary) {
                
                if ( responseDictionary!=nil) {
                    //Valid Data From Server
                    [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
                    
                    NSLog(@"responseDictionary %@",responseDictionary);
                    
                    NSDictionary* userDictionary=[responseDictionary dictionaryByReplacingNullsWithBlanks];
                    
                    
                    completion(TRUE,[[responseDictionary dictionaryByReplacingNullsWithBlanks] mutableCopy]);
                    
                    
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        completion(FALSE,nil);
                    });
                    
                }
                
            }];
            
            
        });
    }else{
        [self showAlertForNoInternet];
    }
    
}


-(void) getTwitteCompletionBlock:(void (^)(NSMutableArray *responseArray))callback{
    
    //[MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
    
    if ([self checkForNetworkAvailability]) {
        
        
      
        
    }else{
        
        [self showAlertForNoInternet];
    }
    
}


- (void) downloadFileFromURL: (NSString *) URL saveTo:(NSURL *)saveToPath withProgress:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSURL *filePath))completionBlock onError:(void (^)(NSError *error))errorBlock
{
    //Configuring the session manager
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    //Most URLs I come across are in string format so to convert them into an NSURL and then instantiate the actual request
    NSURL *formattedURL = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:formattedURL];
    
    
    
    
    //Watch the manager to see how much of the file it's downloaded
    [manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        //Convert totalBytesWritten and totalBytesExpectedToWrite into floats so that percentageCompleted doesn't get rounded to the nearest integer
        CGFloat written = totalBytesWritten;
        CGFloat total = totalBytesExpectedToWrite;
        CGFloat percentageCompleted = written/total;
        
        //Return the completed progress so we can display it somewhere else in app
        progressBlock(percentageCompleted);
    }];
    
    //Start the download
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
//        //Getting the path of the document directory
//        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
//
//        NSURL *fullURL = [documentsDirectoryURL URLByAppendingPathComponent:@"Document.pdf"];
//
//
//       // If we already have a  file saved, remove it from the phone
//        [self removeVideoAtPath:fullURL];
        
        
        return saveToPath;
        
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (!error) {
            //If there's no error, return the completion block
            completionBlock(filePath);
        } else {
            //Otherwise return the error block
            errorBlock(error);
        }
        
    }];
    
    [downloadTask resume];
}

#pragma mark - Server Request

-(void)postServerRequestWithParams:(NSMutableDictionary*)params forUrl:(NSString*)url withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
  //  DLog(@"url : %@   %@",url,params);
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    
     [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-Type"];
    
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    apiLoginManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    apiLoginManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    
//    apiLoginManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
   
    [apiLoginManager POST:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject for url %@ %@",url,responseObject);
        if ([operation.response statusCode] == 200) {
            
            callback(responseObject);
            
        }
        else{
            callback(nil);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error..... %@ ",error);
        callback(nil);
    }];
}



-(void)postServerRequestWithParams:(NSMutableDictionary*)params withAccessToken:(NSString*)token forUrl:(NSString*)url withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
//    DLog(@"url : %@",url);
//    DLog(@"authToken %@",token);
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
  //  apiLoginManager.requestSerializer = [AFJSONRequestSerializer serializer];//Need it for My trip call
    
    [apiLoginManager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //[apiLoginManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    
    [apiLoginManager POST:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //    DLog(@"responseObject for url %@ %ld",responseObject,operation.response.statusCode);
        NSLog(@"responseObject for url %@ %@",url,responseObject);
        
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            if(!responseObject)
            {
                responseObject=[[NSMutableDictionary alloc]init];
                [responseObject setObject:@"1" forKey:@"Success"];
            }
            callback(responseObject);
        }
        else{
           
            callback(nil);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      //   DLog(@"error %@ %@",error, operation.responseString);
      
         if ([operation.response statusCode] == 400) {
             
             
             if([ operation.responseObject objectForKey:@"detail"])
             {
                               }
             else if([ operation.responseObject objectForKey:@"error_description"])
             {
                 
             }
          
             else
             {
                 
                 
             }
             
            
         }
        
        callback(nil);
    }];
}

//Post with image

-(void)postServerRequestForImage:(NSData*)imageData WithParams:(NSDictionary*)params forUrl:(NSString*)url keyValue:(NSString*)keyName andAccessToken:(NSString*)token withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    
    [apiLoginManager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-Type"];
    
    [apiLoginManager POST:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if(imageData)
            [formData appendPartWithFileData:imageData
                                        name:keyName
                                    fileName:@"image.jpeg" mimeType:@"image/jpeg"];
        
        
        
       
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"responseObject for url %@ %@",url,responseObject);
        

        // DLog(@"responseObject %@ %ld",responseObject,(long)[operation.response statusCode]);
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
                 NSLog(@"%@",[responseObject dictionaryByReplacingNullsWithBlanks]);
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
       

        NSLog(@"url %@ error %@ with %@",url,error, operation.responseString);
        callback(nil);
        
    }];
    
    
}
//Get
-(void)getServerRequestForUrl:(NSString*)url withAccessToken:(NSString*)token withResponseCallback:(void (^)(UIImage *responseDictionary))callback
{
    NSLog(@"url : %@",url);
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [apiLoginManager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
//    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-Type"];
//    [apiLoginManager.requestSerializer setValue:@"image/jpeg" forHTTPHeaderField:@"content-Type"];
//
 //   apiLoginManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpeg",@"image/jpg",@"image/png", nil];
    
    apiLoginManager.responseSerializer = [AFImageResponseSerializer serializer];
    
    
    [apiLoginManager GET: [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"responseObject %@ %@",url,responseObject);
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
            UIImage * downloadedImage = responseObject;
            
            callback(downloadedImage);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@ responsestring %@",error, operation.responseString);
        callback(nil);
    }];
}
-(void)getServerRequestForUrl:(NSString*)url withParam:(NSMutableDictionary*)param  withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
   // DLog(@"url : %@",url);
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
  //  [apiLoginManager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [apiLoginManager GET: [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
       //  DLog(@"responseObject %@",responseObject);
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
            callback(responseObject);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      //  DLog(@"error %@ responsestring %@",error, operation.responseString);
        callback(nil);
    }];
}
//Delete
-(void)deleteServerRequestForUrl:(NSString*)url andAccessToken:(NSString*)token withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [apiLoginManager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
   // DLog(@"stringByAddingPercentEncodingWithAllowedCharacters %@",[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]);
    
    [apiLoginManager DELETE:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
      //  DLog(@"responseObject %@ %ld",responseObject,operation.response.statusCode);
        
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
            if(responseObject)
            {
                callback([responseObject dictionaryByReplacingNullsWithBlanks]);
                
            }
            else
            {
                responseObject=[[NSMutableDictionary alloc]init];
                [responseObject setObject:@"Success" forKey:@"Status"];
                callback([responseObject dictionaryByReplacingNullsWithBlanks]);
                
            }
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      //  DLog(@"error %@ operation %@",error, operation.responseString);
        callback(nil);
    }];
}
//Put
-(void)putServerRequestWithParams:(NSDictionary*)params forUrl:(NSString*)url andAccessToken:(NSString*)token withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    apiLoginManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [apiLoginManager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //[apiLoginManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [apiLoginManager PUT:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
       // DLog(@"responseObject %@ url %@ with param %@",responseObject,url,params);
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      //  DLog(@"error %@ %@",error,operation.responseString);
        callback(nil);;
    }];
}
//Put With image
-(void)putServerRequestWithImage:(NSData*)imageData forUrl:(NSString*)url keyValue:(NSString*)keyName param:(NSDictionary*)params andAccessToken:(NSString*)token withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback

{
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    [apiLoginManager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [apiLoginManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //   [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSError *error;
    NSURLRequest *request = [apiLoginManager.requestSerializer multipartFormRequestWithMethod:@"PUT" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if(imageData)
            [formData appendPartWithFileData:imageData
                                        name:keyName
                                    fileName:[NSString stringWithFormat:@"%@_%@.jpeg",keyName,[UserAccount sharedInstance].email ] mimeType:@"image/jpeg"];
        
    } error:&error];
    
    if (error) {
      //  DLog(@"Error on uploading %@",error);
        callback(nil);
        
    }
    
    AFHTTPRequestOperation *operation = [apiLoginManager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
       // DLog(@"responseObject %@ url %@ with param %@",responseObject,url,params);
        
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       // DLog(@"error %@ with %@",error, operation.responseString);
        
        callback(nil);
    }];
    
    [apiLoginManager.operationQueue addOperation:operation];
    //
    
    
}




//Patch
-(void)patchServerRequestWithParams:(NSDictionary*)params forUrl:(NSString*)url andAccessToken:(NSString*)token withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    //DLog(@"params %@ url %@",params,url);
    
    AFHTTPRequestOperationManager *apiLoginManager = [AFHTTPRequestOperationManager manager];
    apiLoginManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [apiLoginManager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [apiLoginManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [apiLoginManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    [apiLoginManager PATCH:[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
      
     //   DLog(@"responseObject %@ %ld",responseObject,operation.response.statusCode);
        
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      //  DLog(@"error %@ %@",error,operation.responseString);
        callback(nil);;
    }];
}

//Patch with image
-(void)patchServerRequestWithImage:(NSData*)imageData forUrl:(NSString*)url keyValue:(NSString*)keyName param:(NSDictionary*)params andAccessToken:(NSString*)token withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    //DLog(@"params %@ url %@",params,url);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSError *error;
    NSURLRequest *request = [manager.requestSerializer multipartFormRequestWithMethod:@"PATCH" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if(imageData)
            [formData appendPartWithFileData:imageData
                                        name:keyName
                                    fileName:[NSString stringWithFormat:@"%@_%@.jpeg",keyName,[UserAccount sharedInstance].email ] mimeType:@"image/jpeg"];
        
    } error:&error];
    
    if (error) {
        // DLog(@"Error on uploading %@",error);
        callback(nil);
       
    }
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
      
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      //  DLog(@"error %@ with %@",error, operation.responseString);
        
        callback(nil);
    }];
    
     [manager.operationQueue addOperation:operation];
    
    
}



//using URL request
-(void)patchServerRequestWith:(NSData*)params forUrl:(NSString*)url andAccessToken:(NSString*)token withResponseCallback:(void (^)(NSDictionary *responseDictionary))callback
{
    //DLog(@"params %@ url %@",params,url);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
 //   NSError *error;
//    NSURLRequest *request = [manager.requestSerializer multipartFormRequestWithMethod:@"PATCH" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//
//
//    } error:&error];
//
//    if (error) {
//        DLog(@"Error on uploading %@",error);
//        callback(nil);
//
//    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:10];
    
    [request setHTTPMethod:@"PATCH"];
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    [request setValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: params];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
       // DLog(@"Result %@ with %ld",responseObject, operation.response.statusCode);
        
        if ([operation.response statusCode] >= 200 && [operation.response statusCode] < 300 ) {
            
            callback([responseObject dictionaryByReplacingNullsWithBlanks]);
            
        }
        else{
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      //  DLog(@"error %@ with %@",error, operation.responseString);
        
        callback(nil);
    }];
    
    [manager.operationQueue addOperation:operation];
    
    
}




- (void)removeVideoAtPath:(NSURL *)filePath
{
    
   
    NSString *stringPath = filePath.path;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:stringPath]) {
        
        NSLog(@"removeVideoAtPath removeVideoAtPath");
        
        [fileManager removeItemAtPath:stringPath error:NULL];
    }
}

- (void)present{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"xxx" object:nil];
}

//Convert Parameter Dictionary to Single string parameter
#pragma mark - Dictionary to String
- (NSString *)urlStringFromDictionary:(NSDictionary*)dict{
    NSArray *keys;
    int i, count;
    id key, value;
    
    keys = [dict allKeys];
    count = (int)[keys count];
    
    NSString *paramString = @"";
    
    for (i = count-1; i >= 0; i--){
        key = [keys objectAtIndex: i];
        value = [dict objectForKey: key];
        if (![paramString isEqualToString:@""])paramString = [paramString stringByAppendingString:@"&"];
        paramString = [paramString stringByAppendingString:[NSString stringWithFormat:@"%@=%@",key,value]];
        
    }
    
    return paramString;
}

#pragma mark - Network Reachability
- (BOOL)checkForNetworkAvailability{
    networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if ((networkStatus != ReachableViaWiFi) && (networkStatus != ReachableViaWWAN)) {
        self.isNetworkAvailable = FALSE;
    }else{
        self.isNetworkAvailable = TRUE;
    }
    
    return self.isNetworkAvailable;
}


- (UIImage*)scaleDown:(UIImage*)img withSize:(CGSize)newSize{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
-(NSData*)compressImageForPP: (UIImage *)img
{
    float MAX_UPLOAD_SIZE=100;
    float MIN_UPLOAD_RESOLUTION=100*100;
    float factor;
    float resol = img.size.height*img.size.width;
    if (resol >MIN_UPLOAD_RESOLUTION){
        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        img = [self scaleDown:img withSize:CGSizeMake(img.size.width/factor, img.size.height/factor)];
    }
    
    //Compress the image
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.5f;
    
    NSData *imageData = UIImageJPEGRepresentation(img, compression);
    
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(img, compression);
       // DLog(@"Compress : %lu",(unsigned long)imageData.length);
    }
    return imageData;
}

-(NSData*)compressImageForPost: (UIImage *)img
{
    float MAX_UPLOAD_SIZE=100;
    float MIN_UPLOAD_RESOLUTION=756*756;
    float factor;
    float resol = img.size.height*img.size.width;
    if (resol >MIN_UPLOAD_RESOLUTION){
        factor = sqrt(resol/MIN_UPLOAD_RESOLUTION)*2;
        img = [self scaleDown:img withSize:CGSizeMake(img.size.width/factor, img.size.height/factor)];
    }
    
    //Compress the image
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    
    NSData *imageData = UIImageJPEGRepresentation(img, compression);
    
    while ([imageData length] > MAX_UPLOAD_SIZE && compression > maxCompression)
    {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(img, compression);
       // DLog(@"Compress : %lu",(unsigned long)imageData.length);
    }
    return imageData;
}

//server not available
- (void)showAlertForNoInternet{
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No internet connection available" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        alert = nil;
        
        [MBProgressHUD hideHUDForView:APP_DELEGATE.window animated:YES];
        
        //[iSnackBar snackBarWithMessage:iSnackBarMessage action:@"Undo" delegate:self];
        
        
    });
}

@end
