//
//  AppSupport.h
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

@interface Routes : NSObject

+(void)setLoaderAsRootViewController;

+(void)gotoTabViewFromVC:(UIViewController*)vc;
+(void)gotoLoginViewFromVC:(UIViewController*)vc;
+(void)gotoRegistrationViewFromVC:(UIViewController*)vc;
+(void)gotoSecondRegistrationViewFromVC:(UIViewController*)vc withDic:(NSMutableDictionary*)datadic;
+(void)gotoThirdRegistrationViewFromVC:(UIViewController*)vc withDic:(NSMutableDictionary*)datadic;;
+(void)gotoResourceViewFromVC:(UIViewController*)vc;
+(void)gotoSpeakerViewFromVC:(UINavigationController*)vc;
+(void)gotoContactViewFromVC:(UINavigationController*)vc;

+(void)popToLoginViewControllerFromVC:(UIViewController*)vc;
+(void)gotoWebContainerViewFromVC:(UINavigationController*)vc forUrlString:(NSString*)urlString;


+(void)gotoChatViewFromVC:(UINavigationController*)vc withDictionary:(NSMutableDictionary*)dataDic;

+(void)gotoLoginViewFrom:(UIViewController*)vc;
@end
