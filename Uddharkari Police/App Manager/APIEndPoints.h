//
//  StorageAPI.h
//  DeblahApp
//
//  Created by Sabuj on 20/4/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//
#import <UIKit/UIKit.h>

struct BaseURL {
    
    __unsafe_unretained NSString* const Development;
    __unsafe_unretained NSString* const Production;
    __unsafe_unretained NSString* const ChatBaseUrl;
};

struct Api {
    //registration
    
    __unsafe_unretained NSString* const registration;
    __unsafe_unretained NSString* const driverRegistration;
    //Login
    __unsafe_unretained NSString* const signIn;
    __unsafe_unretained NSString* const logout;
    __unsafe_unretained NSString* const fcmRegistration;
    __unsafe_unretained NSString* const changePassword;

    
    __unsafe_unretained NSString* const ownProfileInfo;
    __unsafe_unretained NSString* const updateProfileInfo;
    __unsafe_unretained NSString* const history;
    __unsafe_unretained NSString* const updateRespondentLocation;
    __unsafe_unretained NSString* const uploadProPic;
    __unsafe_unretained NSString* const downloadProPic;

    
    __unsafe_unretained NSString* const nationalityList;
    __unsafe_unretained NSString* const goodTypeList;
    __unsafe_unretained NSString* const deliveryTypeList;
    
    __unsafe_unretained NSString* const issueCategories;
    __unsafe_unretained NSString* const issue;
    
    
    //Trip
    __unsafe_unretained NSString* const tripRating;
    __unsafe_unretained NSString* const tripPayment;
    __unsafe_unretained NSString* const penaltyPayment;
    __unsafe_unretained NSString* const verifyPenaltyPayment;
    
    __unsafe_unretained NSString* const recurringTrip;
    __unsafe_unretained NSString* const myTrip;
    __unsafe_unretained NSString* const tripEstimation;
    __unsafe_unretained NSString* const tripIssue;
    
    __unsafe_unretained NSString* const currentTrip;
    __unsafe_unretained NSString* const vehicleLocation;
    
};

extern const struct BaseURL BaseURL;
extern const struct Api Api;

@interface APIEndPoints : NSObject

@end
