//
//  AppSupport.h
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AppState) {
    Online= 0,
    Driver_On_The_Way = 1,
    Trip_Started = 2,
    Trip_Completed = 3,
    Trip_Canceled = 4,
    Trip_Accepted = 5,
    Req_Pending = 6,
    Payment_Completed=7,
    Driver_Not_Found = 9
};

typedef NS_ENUM(NSInteger, DeliveryType) {
    Normal = 0,
    Urgent = 1
};

typedef NS_ENUM(NSInteger, RecurrenceType) {
    Once = 0,
    Daily = 1,
    Weekly = 2,
    Monthly = 3
};

typedef NS_ENUM(NSInteger, UserState) {
    UserOnline = 0,
    UserOffline = 1
};

@interface AppUtility : NSObject

+(BOOL) mailAuthentication:(NSString *)mailText;
+(BOOL) phoneAuthentication:(NSString *)phoneText;
+ (BOOL) fieldIsEmpty:(NSString*) inputString;

+(NSInteger) nextPageParser:(NSString *)str;


+(NSString*) getDateStringFromDate:(NSDate*)date;
+(NSDate*) getNSDateFromServerDate:(NSString*)serverDateString;
+(NSString*) getMyTripStyleDateStringFromDate:(NSDate*)date;

+ (NSString *)getPriceTextforCurrency:(NSString*)price;
+ (NSString *)getFirstObjectFromArray:(NSArray*)array;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;

+ (NSMutableDictionary *)extractLocationDataFromString:(NSString *)locationString;
+ (UIViewController*) topMostViewController;

+(NSString* ) getCSRFFromCookie;

@end
