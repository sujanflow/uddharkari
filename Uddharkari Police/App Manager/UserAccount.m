//
//  Recipe.m
//  RecipeApp
//
//  Created by Simon on 25/12/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "UserAccount.h"
#import "NSString+Utils.h"

#define TimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]

@interface UserAccount ()

@property (nonatomic, retain) NSUserDefaults *userDefaults;

@end


@implementation UserAccount

@synthesize userDefaults = _userDefaults;

+(UserAccount *)sharedInstance
{
    static UserAccount *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[UserAccount alloc] init];
    });
    return _sharedInstance;
}


#pragma mark - init
- (id)init{
    if (self = [super init]){
        
        self.userDefaults = [NSUserDefaults standardUserDefaults];
        // Set some defaults for the first run of the application
    
       // self.lastMessageDate=@"";
       // self.lastActivityDate=@"";
        
        [self.userDefaults synchronize];
    }
    return self;
}



-(int)userId{
    return [[self.userDefaults stringForKey:@"userId"] intValue];
}

- (void)setUserId:(int)value
{
    [self.userDefaults setInteger:value forKey:@"userId"];
    [self.userDefaults synchronize];
}

-(float)penaltyAmount{
    return [[self.userDefaults valueForKey:@"penaltyAmount"] floatValue];
}

- (void)setPenaltyAmount:(float)value
{
    [self.userDefaults setFloat:value forKey:@"penaltyAmount"];
    [self.userDefaults synchronize];
}

-(float)rating{
    return [[self.userDefaults valueForKey:@"rating"] floatValue];
}

- (void)setRating:(float)value
{
    [self.userDefaults setFloat:value forKey:@"rating"];
    [self.userDefaults synchronize];
}

-(NSString*) userImageName
{
    return [self.userDefaults objectForKey:@"userImageName"];
}

- (void)setUserImageName:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"userImageName"];
    [self.userDefaults synchronize];
}

-(NSString*)userName
{
    return [self.userDefaults objectForKey:@"userName"];
}

- (void)setUserName:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"userName"];
    [self.userDefaults synchronize];
}


-(NSString*)email
{
    return [self.userDefaults objectForKey:@"email"];
}

- (void)setEmail:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"email"];
    [self.userDefaults synchronize];
}


-(NSString*)paymentStatus
{
    return [self.userDefaults objectForKey:@"paymentStatus"];
}

- (void)setPaymentStatus:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"paymentStatus"];
    [self.userDefaults synchronize];
}

-(NSString*) phoneNumber
{
    return [self.userDefaults objectForKey:@"phoneNumber"];
}

- (void)setPhoneNumber:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"phoneNumber"];
    [self.userDefaults synchronize];
}

-(NSString*) address
{
    return [self.userDefaults objectForKey:@"address"];
}

- (void)setAddress:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"address"];
    [self.userDefaults synchronize];
}

-(NSMutableDictionary*)location
{
    return [self.userDefaults objectForKey:@"location"];
}

- (void)setLocation:(NSMutableDictionary *)dictionary
{
    [self.userDefaults setObject:dictionary forKey:@"location"];
    [self.userDefaults synchronize];
}

-(NSString*) gender
{
    return [self.userDefaults objectForKey:@"gender"];
}

- (void)setGender:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"gender"];
    [self.userDefaults synchronize];
}

-(NSString*) birthDate
{
    return [self.userDefaults objectForKey:@"birthDate"];
}

- (void)setBirthDate:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"birthDate"];
    [self.userDefaults synchronize];
}

-(NSString*)accessToken
{
    return [self.userDefaults objectForKey:@"accessToken"];
}

- (void)setAccessToken:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"accessToken"];
    [self.userDefaults synchronize];
}

-(NSString*)refreshToken
{
    return [self.userDefaults objectForKey:@"refreshToken"];
}

- (void)setRefreshToken:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"refreshToken"];
    [self.userDefaults synchronize];
}


-(NSString*)deviceToken
{
    return [[self.userDefaults objectForKey:@"deviceToken"] isValid]?[self.userDefaults objectForKey:@"deviceToken"]:TimeStamp;
}

- (void)setDeviceToken:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"deviceToken"];
    [self.userDefaults synchronize];
}

-(NSString*)gcmRegKey
{
    return [self.userDefaults objectForKey:@"gcmRegKey"];
}

- (void)setGcmRegKey:(NSString *)value
{
    [self.userDefaults setObject:value forKey:@"gcmRegKey"];
    [self.userDefaults synchronize];
}


-(BOOL)isLoggedIn
{
    NSLog(@"self.accessToken %d",[self.accessToken isValid]);
     return [self.accessToken isValid];
}



- (NSMutableDictionary *)toNSDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setObject:[NSString stringWithFormat:@"%@",self.accessToken] forKey:@"access_token"];
    [dictionary setObject:[NSString stringWithFormat:@"%@",self.refreshToken] forKey:@"refreshToken"];
    
    [dictionary setObject:[NSString stringWithFormat:@"%@",self.email] forKey:@"email"];
    
    [dictionary setValue:[NSNumber numberWithInt:self.userId] forKey:@"user_id"];
    [dictionary setValue:[NSString stringWithFormat:@"%@",self.userName] forKey:@"name"];
  
    [dictionary setObject:[NSString stringWithFormat:@"%@",self.phoneNumber] forKey:@"phoneNumber"];
    [dictionary setObject:[NSString stringWithFormat:@"%@",self.address] forKey:@"address"];
    
    [dictionary setValue:self.location forKey:@"location"];
    [dictionary setObject:[NSString stringWithFormat:@"%@",self.birthDate] forKey:@"birthDate"];
    [dictionary setObject:[NSString stringWithFormat:@"%@",self.gender] forKey:@"gender"];
    [dictionary setObject:[NSString stringWithFormat:@"%@",self.gcmRegKey] forKey:@"fcm"];
    
    
    return dictionary;

}

-(void)removeUser
{
    
    self.accessToken=self.refreshToken=nil;

    self.userImageName=self.userName=self.email=self.phoneNumber=self.address=self.gender=self
    .birthDate=nil;
    self.location=nil;
    
    self.userId=0;
    
}




@end
