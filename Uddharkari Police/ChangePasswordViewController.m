//
//  ForgetPasswordViewController.m
//  Uddharkari Police
//
//  Created by Md.Ballal Hossen on 30/4/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "ServerManager.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)changePassButtonAction:(id)sender {
    
    if (self.latestPassTextField.text.length > 0) {
        
    
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
    
    [userInfo setObject:self.latestPassTextField.text forKey:@"new_password"];
    
//    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
//                                                       options:NSJSONWritingPrettyPrinted error:nil];
//    
//    
//    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    
//    NSString *newString = [[newStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
//    
//    NSLog(@"tempString %@",newString);
//    
//    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
//    
//    [param setObject:newString forKey:@""];
    
    [[ServerManager sharedManager] changePassword:(NSMutableDictionary *)userInfo withCompletionBlock:^(BOOL success) {
        
        if (success) {
            
            NSLog(@"changePassword success");
        }
        
    }];
    }
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField{
//
//    [textField resignFirstResponder];
//
//    return YES;
//}
//


- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
