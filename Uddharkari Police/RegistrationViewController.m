//
//  RegistrationViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "RegistrationViewController.h"
#import "KYDrawerController.h"
#import "ServerManager.h"

@interface RegistrationViewController ()<IQDropDownTextFieldDelegate>{
    
    NSArray *dropDownArray;
}

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
 
     self.genderTextField.showDismissToolbar = YES;
    [self.genderTextField setItemList:[NSArray arrayWithObjects:@"M",@"F", nil]];
    [self.genderTextField setItemListUI:[NSArray arrayWithObjects:@"M",@"F", nil]];

    self.stationTextField.showDismissToolbar = YES;
    [self.stationTextField setItemList:[NSArray arrayWithObjects:@"Kalabagan Police Statio",@"New Market Police Station",@"Shahbagh Police Station",@"Dhanmondi Model Thana",@"Ramna Model Police Station",@"Bunga Police Station",@"Police Fari", @"Khilgaon Police Station",@"Mohammadpur Police Station",@"Tejgaon Police Station",@"Bangshal Police Station",nil]];
    [self.stationTextField setItemListUI:[NSArray arrayWithObjects:@"Kalabagan Police Statio",@"New Market Police Station",@"Shahbagh Police Station",@"Dhanmondi Model Thana",@"Ramna Model Police Station",@"Bunga Police Station",@"Police Fari",@"Khilgaon Police Station",@"Mohammadpur Police Station",@"Tejgaon Police Station",@"Bangshal Police Station", nil]];

    self.designationTextField.showDismissToolbar = YES;
    [self.designationTextField setItemList:[NSArray arrayWithObjects:@"Constable",@"Inspector",@"Sergent",@"PatrolOfficer",@"SP", nil]];
    [self.designationTextField setItemListUI:[NSArray arrayWithObjects:@"Constable",@"Inspector",@"Sergent",@"PatrolOfficer",@"SP", nil]];

    
}

-(void)textField:(nonnull IQDropDownTextField*)textField didSelectItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
}
-(BOOL)textField:(nonnull IQDropDownTextField*)textField canSelectItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
    return YES;
}

-(IQProposedSelection)textField:(nonnull IQDropDownTextField*)textField proposedSelectionModeForItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
    return IQProposedSelectionBoth;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
    
    if (textField == self.firstnameTestField) {
        
        if (textField.text.length < 3) {
            
            NSLog(@"First name must have at least 3 carc.");
        }
        
    }else if (textField == self.lastNameTextField){
        
        if (textField.text.length < 3) {
            
            NSLog(@"Last name must have at least 3 carc.");
        }
    }else if (textField == self.ageTextField){
        
        if ([textField.text doubleValue] < 23) {
            
            NSLog(@"Age can not be less than 23");
        }
        
    }
}

-(void)doneClicked:(UIBarButtonItem*)button
{
    [self.view endEditing:YES];
    
//    NSLog(@"textFieldTextPicker.selectedItem: %@", self.contactRelationTextField.selectedItem);
//    
}

- (IBAction)registerButtonAction:(id)sender {
    
    
    NSMutableDictionary*temp = [[NSMutableDictionary alloc]init];
    
    [temp setObject:self.firstnameTestField.text forKey:@"first_name"];
    [temp setObject:self.lastNameTextField.text forKey:@"last_name"];
    [temp setObject:self.ageTextField.text forKey:@"age"];
   
    [temp setObject:self.stationTextField.text forKey:@"station_name"];

    [temp setObject:self.genderTextField.text forKey:@"gender"];
    [temp setObject:self.contactNoTF.text forKey:@"contact_number"];
    [temp setObject:self.designationTextField.text forKey:@"designation"];
    [temp setObject:self.passwordTF.text forKey:@"password"];
    [temp setObject:self.contactNoTF.text forKey:@"username"];
    
     NSLog(@"temp.........%@",temp);
    
    
    [[ServerManager sharedManager] postSignUpWithDictionary:temp completion:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        if (success) {
            
            NSLog(@"registration success %@",resultDataDic);
            
            UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:Nil];
            KYDrawerController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"KYDrawerController"];
            [self.navigationController pushViewController:destinationVC animated:YES];
            
        }
        
        
    }];

    
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
