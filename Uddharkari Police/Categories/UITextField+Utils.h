//
//  UITextField+Utils.h
//  UberGoods Customer
//
//  Created by Tanvir Palash on 30/6/18.
//  Copyright © 2018 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Utils)

- (void)makeCircleForBorderWidth:(CGFloat)width;


- (void)makeRound;
- (void)setCustomTextRect;

-(void)setRightPaddingView:(CGFloat)amount;
- (void)setPlaceholder:(NSString*)text withColor:(UIColor*)color;


@end
