//
//  HomeViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "HomeViewController.h"
#import "KYDrawerController.h"
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>
#import "Reachability.h"
#import "ServerManager.h"
#import "UserAccount.h"
#import "SocketRocket.h"

@interface HomeViewController ()<CLLocationManagerDelegate,SRWebSocketDelegate>{
    
    CLLocationManager *locationManager;
    
    CLLocationCoordinate2D currentLocation;
    
    Reachability *networkReachability;
    SRWebSocket *_webSocket;
    
    NSString *phoneNo;
    
    
    BOOL isRegistaringSocket;
    
    NSString* victimLatitude;
    NSString* victimLongitude;
    NSString* victimPhone;
    
}
@property (nonatomic, readwrite) BOOL isNetworkAvailable;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotHelpRequest:) name:@"gotHelpRequest" object:nil];
    
    phoneNo = [[NSUserDefaults standardUserDefaults] objectForKey:@"phoneNo"];
    
    
    
    [[ServerManager sharedManager] postLoginWithPhone:phoneNo password:[[NSUserDefaults standardUserDefaults] objectForKey:@"passWord"] completion:^(BOOL success) {
        
        if (success) {
            
            NSLog(@"success.........");
            
            NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
            
            [userInfo setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"registrationToken"] forKey:@"userToken"];
            
//            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
//                                                               options:NSJSONWritingPrettyPrinted error:nil];
//
//
//            NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//
//            NSString *newString = [[newStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
//
//            NSLog(@"tempString %@",newString);
//
//            NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
//
//            [param setObject:newString forKey:@""];
            
            [[ServerManager sharedManager] postFCMToken:(NSMutableDictionary *)userInfo withCompletionBlock:^(BOOL success) {
                
                if (success) {
                    
                    NSLog(@"fcm success");
                }
                
            }];

        }
            
    }];
    
}
- (void)viewWillAppear:(BOOL)animated{
    
    [self getLocation];
    
    
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)getLocation{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
}

//--------locationmanager delegate to get current location-------------//
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    
    CLLocation *currentPostion=locations.lastObject;
    
    //CLLocation *currentPostion=locations.lastObject;
    currentLocation.latitude=currentPostion.coordinate.latitude;
    currentLocation.longitude=currentPostion.coordinate.longitude;
    
    NSLog(@"Current Location = %f, %f",currentLocation.latitude,currentLocation.longitude);
    
     [manager stopUpdatingLocation];
}

- (IBAction)menuButtonAction:(id)sender {
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
}



-(void)gotHelpRequest:(NSNotification*)notification
{
    
    NSDictionary* userInfo = notification.userInfo;
    
    NSLog(@"notificationnotificationnotificationnotificationnotification %@",userInfo);
    
    
    self.notificationView.hidden = NO;
    
    self.firstName.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"victim_first_name"]];
    self.lastName.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"victim_last_name"]];
    self.age.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"victim_age"]];
    self.emgContactNo.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"victim_emergency_contact"]];
    self.emgContactRelation.text = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"victim_emergency_relation"]];
    
    victimPhone = [userInfo objectForKey:@"victim_contact_number"];
    victimLatitude = [userInfo objectForKey:@"latitude"];
    victimLongitude = [userInfo objectForKey:@"longitude"];
    
}

- (IBAction)callVictimButtonAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",victimPhone]]];

    
}

- (IBAction)victimLocationButtonAction:(id)sender {
    //temp
//    victimLatitude = @"23.7440726";
//    victimLongitude = @"90.3822616";
    
    
   NSString* currentlat = [NSString stringWithFormat:@"%f",currentLocation.latitude];
    NSString* currentlong = [NSString stringWithFormat:@"%f",currentLocation.longitude];
    
    NSString* link = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%@,%@&daddr=%@,%@",
                                     currentlat, currentlong,victimLatitude,victimLongitude];
    
    //http://maps.google.com/?saddr=34.052222,-118.243611&daddr=37.322778,-122.031944
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
    

    
}

- (IBAction)rescueButtonAction:(id)sender {
    
    if ([self checkForNetworkAvailability])  {
        
        NSLog(@"...... net");
        
        _webSocket.delegate = nil;
        
        [_webSocket close];
        
        
        
        NSString *socketString = [NSString stringWithFormat:@"ws://104.192.5.213/respondent?path=/openRespondentSocket&number=%@",phoneNo];
        
        _webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:socketString]];
        
        _webSocket.delegate = self;
        
        
        [_webSocket open];
        
        
    }
}

///--------------------------------------
#pragma mark - SRWebSocketDelegate
///--------------------------------------

- (void)webSocketDidOpen:(SRWebSocket *)webSocket;
{
    NSLog(@"Websocket Connected");
    
    self.rescueButton.hidden = YES;
    self.caseCompleteButton.hidden = NO;
    
    
   // victimPhone = @"+8801516515911";
    NSString* currentlat = [NSString stringWithFormat:@"%f",currentLocation.latitude];
    NSString* currentlong = [NSString stringWithFormat:@"%f",currentLocation.longitude];
    
    NSDictionary*temp = @{@"path": @"/respondentCaseAccept",@"number": phoneNo,@"latitude":currentlat,@"longitude":currentlong,@"victim_number":victimPhone};

    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:temp
                                                       options:NSJSONWritingPrettyPrinted error:nil];


    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    NSLog(@"tempString %@",newStr);

    isRegistaringSocket = YES;

    [_webSocket sendString:newStr error:NULL];
    
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error;
{
    NSLog(@":( Websocket Failed With Error %@", error);
    
    self.title = @"Connection Failed! (see logs)";
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(nonnull NSString *)string
{
    NSLog(@"Received \"%@\"", string);
    
    if (isRegistaringSocket) {

        [self timerCalled];
    }
    
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if ([json objectForKey:@"message"] != nil) {
        
        if ([[json objectForKey:@"message"] isEqualToString:@"Case Closed"]) {
            
            NSLog(@"Case Closed..............");
            
            self.notificationView.hidden = YES;
            self.caseCompleteButton.hidden = YES;
        }
    }    
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean;
{
    NSLog(@"WebSocket closed reson %@",reason);
    self.title = @"Connection Closed! (see logs)";
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload;
{
    NSLog(@"WebSocket received pong");
}

-(void)timerCalled
{
    NSLog(@"Timer Called");
    
    NSString*lat =[NSString stringWithFormat:@"%f",currentLocation.latitude];
    NSString*lng =[NSString stringWithFormat:@"%f",currentLocation.longitude];
    
    
    NSDictionary*temp = @{@"path": @"/updateRespondentLocation",@"number": phoneNo,@"latitude": lat,@"longitude": lng};
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:temp
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    
    
    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"tempString %@",newStr);
    
    isRegistaringSocket = NO;
    
    [_webSocket sendString:newStr error:NULL];
    
}


- (IBAction)caseCompletedButtonAction:(id)sender {
    
    
    //closeVictimSocket
    //close socket
    
    NSDictionary*temp1 = @{@"path": @"/closeCase",@"number": phoneNo,@"victim_number": victimPhone};
    
        NSData* jsonData1 = [NSJSONSerialization dataWithJSONObject:temp1
                                                           options:NSJSONWritingPrettyPrinted error:nil];
    
    
        NSString* newStr1 = [[NSString alloc] initWithData:jsonData1 encoding:NSUTF8StringEncoding];
    
        NSLog(@"tempString %@",newStr1);
    
        [_webSocket sendString:newStr1 error:NULL];
    
    //...........
    

    
}



#pragma mark - Network Reachability
- (BOOL)checkForNetworkAvailability{
    networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if ((networkStatus != ReachableViaWiFi) && (networkStatus != ReachableViaWWAN)) {
        self.isNetworkAvailable = FALSE;
    }else{
        self.isNetworkAvailable = TRUE;
    }
    
    return self.isNetworkAvailable;
}

@end
