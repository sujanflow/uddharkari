//
//  AboutViewController.m
//  Uddharkari Police
//
//  Created by Md.Ballal Hossen on 30/4/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "AboutViewController.h"
#import "KYDrawerController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)menuButtonAction:(id)sender {
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
}


@end
