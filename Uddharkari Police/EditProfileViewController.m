//
//  EditProfileViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 19/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "EditProfileViewController.h"
#import "ServerManager.h"

@interface EditProfileViewController ()<IQDropDownTextFieldDelegate>

@end
//designationTextField
@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.designationTextField.showDismissToolbar = YES;
    [self.designationTextField setItemList:[NSArray arrayWithObjects:@"Constable",@"Inspector",@"Sergent",@"PatrolOfficer",@"SP", nil]];
    [self.designationTextField setItemListUI:[NSArray arrayWithObjects:@"Constable",@"Inspector",@"Sergent",@"PatrolOfficer",@"SP", nil]];

    self.designationTextField.text = self.designation;
    
}

-(void)textField:(nonnull IQDropDownTextField*)textField didSelectItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
}
-(BOOL)textField:(nonnull IQDropDownTextField*)textField canSelectItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
    return YES;
}

-(IQProposedSelection)textField:(nonnull IQDropDownTextField*)textField proposedSelectionModeForItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
    return IQProposedSelectionBoth;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

-(void)doneClicked:(UIBarButtonItem*)button
{
    [self.view endEditing:YES];
    
//    NSLog(@"textFieldTextPicker.selectedItem: %@", self.designationTextField.selectedItem);
    
}

- (IBAction)updateButton:(id)sender {
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
    
    [userInfo setObject:self.designationTextField.text forKey:@"designation"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    
    
    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSString *newString = [[newStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    
    NSLog(@"tempString %@",newString);
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    
    [param setObject:newString forKey:@"updates"];
    
    
    [[ServerManager sharedManager]updateCurrentProfileDetailswith:param withBlock:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        NSLog(@"????%@",resultDataDic);
        
    }];
}



- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
